﻿using Joedmin.Mimir.Scrapper.Api.Models;
using Joedmin.Mimir.Scrapper.Api.Utils;

namespace Joedmin.Mimir.Scrapper.Api.Services;

public class ScrapperService
{
	private readonly HttpClient httpClient;

	public ScrapperService(HttpClient httpClient)
	{
		this.httpClient = httpClient;
	}

	public async Task<BookOverview[]> GetBooksFromQuerryAsync(
		string titleQuerry,
		int page,
		CancellationToken cancellationToken)
	{
		var uri = GoodreadsConstants.SearchPath + Uri.EscapeDataString(titleQuerry) + GoodreadsConstants.PageQuery + page;
		var html = await httpClient.GetStringAsync(uri, cancellationToken).ConfigureAwait(false);

		var document = HtmlUtils.Load(html);
		var foundBooks = document.DocumentNode.SelectNodes("//tr");

		return HtmlUtils.ParseBookNodes(foundBooks);
	}
}
