using System.Text.Json.Serialization;
using Joedmin.Mimir.Scrapper.Api.Models;
using Joedmin.Mimir.Scrapper.Api.Services;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateSlimBuilder(args);

builder.Services.ConfigureHttpJsonOptions(options =>
{
	options.SerializerOptions.TypeInfoResolverChain.Insert(0, AppJsonSerializerContext.Default);
});

builder.Services.AddHttpClient<ScrapperService>();

// TODO: Transient vs scoped?
builder.Services.AddTransient<ScrapperService>();

var app = builder.Build();

app.MapGet("/", () => "Hello world!");
app.MapGet("/search/{searchParam}", async (
	string searchParam,
	ScrapperService scrapper,
	CancellationToken cancellationToken,
	[FromQuery] int page = 1) => await scrapper.GetBooksFromQuerryAsync(searchParam, page, cancellationToken));

//var booksApi = app.MapGroup("/book");

await app.RunAsync();

[JsonSerializable(typeof(BookOverview[]))]
[JsonSerializable(typeof(BookDetail[]))]
internal partial class AppJsonSerializerContext : JsonSerializerContext
{

}
