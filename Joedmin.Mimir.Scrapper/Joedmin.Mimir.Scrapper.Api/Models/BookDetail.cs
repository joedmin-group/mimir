﻿namespace Joedmin.Mimir.Scrapper.Api.Models;

public class BookDetail
{
	public string Title { get; set; }

	public string Description { get; set; }

	public DateOnly FirstPublished { get; set; }

	public float AverageRating { get; set; }

	public int TotalRatings { get; set; }

	public int TotalReviews { get; set; }

	public ICollection<Author> Authors { get; set; }

	public ICollection<string> Genres { get; set; }

	public Uri ImageUri { get; set; }

	public Uri DetailsUri { get; set; }
}
