﻿namespace Joedmin.Mimir.Scrapper.Api.Models;

public class BookOverview
{
	public string Title { get; set; }

	public int PublishedYear { get; set; }

	public float AverageRating { get; set; }

	public int TotalRatings { get; set; }

	public int EditionAmount { get; set; }

	public AuthorOverview[] Authors { get; set; }

	public Uri? ImageUri { get; set; }

	public Uri? DetailsUri { get; set; }
}

public class AuthorOverview
{
	public string Name { get; set; }

	public string? Role { get; set; } = string.Empty;

	public Uri? Uri { get; set; }
}
