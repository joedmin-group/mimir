﻿using System.Xml.Linq;
using HtmlAgilityPack;
using Joedmin.Mimir.Scrapper.Api.Models;

namespace Joedmin.Mimir.Scrapper.Api.Utils;

public static class HtmlUtils
{
	public static HtmlDocument Load(string html)
	{
		var document = new HtmlDocument();
		document.LoadHtml(html);
		return document;
	}

	public static BookOverview[] ParseBookNodes(HtmlNodeCollection? nodes)
	{
		if (nodes is null)
		{
			return [];
		}

		var parsed = new BookOverview[nodes.Count];
		for (int i = 0; i < nodes.Count; i++)
		{
			var node = nodes[i];
			parsed[i] = new BookOverview()
			{
				Title = node.SelectNodes("descendant::span[@itemprop=\"name\"]")[0].InnerHtml,
				Authors = GetAuthors(node.SelectNodes("descendant::div[@class=\"authorName__container\"]")),
				//AverageRating = node.SelectNodes("descendant::span[@itemprop=\"name\"]")[0].InnerHtml,
				ImageUri = GetImageUri(node),
				DetailsUri = GetLinkUri(node),
			};
		}

		return parsed;
	}

	private static AuthorOverview[] GetAuthors(HtmlNodeCollection? nodes)
	{
		if (nodes is null)
		{
			return [];
		}

		var parsed = new AuthorOverview[nodes.Count];
		for (int i = 0; i < nodes.Count; i++)
		{
			var node = nodes[i];
			parsed[i] = new AuthorOverview()
			{
				Name = node.SelectNodes("descendant::span[@itemprop=\"name\"]")[0].InnerHtml,
				Uri = GetLinkUri(node),
				Role = node.SelectNodes("descendant::span[@class=\"greyText\"]")?[0].InnerHtml ?? string.Empty,
			};
		}

		return parsed;
	}

	private static Uri GetUri(string uri)
		=> new(GoodreadsConstants.Uri, uri);

	private static Uri GetImageUri(HtmlNode node)
		=> GetUri(node.SelectNodes("descendant::img")[0].Attributes["src"].Value);

	private static Uri GetLinkUri(HtmlNode node)
		=> GetUri(node.SelectNodes("descendant::a")[0].Attributes["href"].Value);
}
