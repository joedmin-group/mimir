﻿namespace Joedmin.Mimir.Scrapper.Api;

public static class GoodreadsConstants
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S1075:URIs should not be hardcoded", Justification = "Intended use.")]
	public static readonly Uri Uri = new("https://www.goodreads.com");

	public static readonly Uri SearchPath = new(Uri, "/search?utf8=✓&q=");

	public const string PageQuery = "&page=";
}
